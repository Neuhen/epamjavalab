package functions;

import models.Tariff;
import utilits.TariffComparator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

public class Functions {

    private static final Logger logger = Logger.getLogger(Functions.class.getName());

    private static int  totalNumberOfCustomers = 0;

    public static int getTotalNumberOfCustomers(ArrayList<Tariff> tariffs){



        for (Tariff tariff: tariffs)
        {
            totalNumberOfCustomers += tariff.getNumberOfCustomers();
        }
        logger.info("colled getTotalNumberOfCustomers method");
        return totalNumberOfCustomers;
    }

    public static List<Tariff> sortBySubscriptionFee(List<Tariff> tariffs){
        logger.info("colled sortBySubscriptionFee method");

        TariffComparator tariffComparator = new TariffComparator();

        Collections.sort(tariffs, tariffComparator);
        for (Tariff tarrif: tariffs) {
            System.out.println(tarrif.getSubscriptionFee());
        }

        return tariffs;
    }

    public static Tariff searchByParameters(List<Tariff> tariffs, int costFrom, int costTo, int numberOfCustomersFrom, int numberOfCustomersTo){

        Tariff tariff1 = null;
        for (Tariff tariff: tariffs) {
            if(costTo >= tariff.getSubscriptionFee() && tariff.getSubscriptionFee() >= costFrom && tariff.getNumberOfCustomers() <= numberOfCustomersTo && tariff.getNumberOfCustomers() >= numberOfCustomersFrom){
                tariff1 = tariff;
            }
        }
        logger.info("colled searchByParameters method");
        return tariff1;
    }
}
