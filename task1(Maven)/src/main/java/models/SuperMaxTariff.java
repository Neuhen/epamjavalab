package models;

import java.util.logging.Logger;

public class SuperMaxTariff extends Tariff {

    private static final Logger logger = Logger.getLogger(SuperMaxTariff.class.getName());

    public SuperMaxTariff(int numberOfCustomers, int subscriptionFee){

        super(numberOfCustomers, subscriptionFee);
        logger.info("created SuperMaxTariff object");
    }

}
