package models;

import java.util.logging.Logger;

public abstract class Tariff {
    private int numberOfCustomers;
    private int subscriptionFee;

    protected Tariff(int numberOfCustomers, int subscriptionFee){

        this.numberOfCustomers = numberOfCustomers;
        this.subscriptionFee = subscriptionFee;
    }

    public int getNumberOfCustomers(){
        return numberOfCustomers;
    }

    public int getSubscriptionFee(){
        return subscriptionFee;
    }

}
