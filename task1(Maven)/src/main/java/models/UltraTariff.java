package models;

import java.util.logging.Logger;

public class UltraTariff extends Tariff {

    private static final Logger logger = Logger.getLogger(UltraTariff.class.getName());

    public UltraTariff(int numberOfCustomers, int subscriptionFee){

        super(numberOfCustomers, subscriptionFee);
        logger.info("created Ultratariff object");

    }


}
