package models;

import java.util.logging.Logger;

public class UnlimitedTariff extends Tariff {

    private static final Logger logger = Logger.getLogger(UltraTariff.class.getName());

    public UnlimitedTariff(int numberOfCustomers, int subscriptionFee) {

        super(numberOfCustomers, subscriptionFee);

        logger.info("created SuperMaxTariff object");
    }
}
