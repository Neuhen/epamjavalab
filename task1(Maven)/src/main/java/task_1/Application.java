package task_1;

import functions.Functions;
import models.SuperMaxTariff;
import models.Tariff;
import models.UltraTariff;
import models.UnlimitedTariff;

import java.util.ArrayList;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class Application {

    private static final Logger logger = Logger.getLogger(Application.class.getName());

    public static void main(String[] args) {

        try {
            LogManager.getLogManager().readConfiguration();
        }catch (Exception e){
            logger.warning("error reading logger configuration ");
        }
        ArrayList<Tariff> tariffs = new ArrayList<Tariff>();

        UnlimitedTariff unlimitedTariff = new UnlimitedTariff(503, 20);
        SuperMaxTariff superMaxTariff = new SuperMaxTariff(400, 15);
        UltraTariff ultraTariff = new UltraTariff(10, 30);

        tariffs.add(unlimitedTariff);
        tariffs.add(superMaxTariff);
        tariffs.add(ultraTariff);

        System.out.println(Functions.getTotalNumberOfCustomers(tariffs));
        Functions.sortBySubscriptionFee(tariffs);
        Functions.searchByParameters(tariffs, 18, 25, 50, 200);

    }
}

