package utilits;

import models.Tariff;

import java.util.Comparator;

public class TariffComparator implements Comparator<Tariff> {

    public int compare(Tariff tariff1, Tariff tariff2){

        if(tariff1.getSubscriptionFee()> tariff2.getSubscriptionFee())
            return 1;
        else if(tariff1.getSubscriptionFee()< tariff2.getSubscriptionFee())
            return -1;
        else
            return 0;
    }
}
