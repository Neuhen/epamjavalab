package functions;

import models.SuperMaxTariff;
import models.Tariff;
import models.UltraTariff;
import models.UnlimitedTariff;
import org.junit.*;

import java.util.ArrayList;
import java.util.List;

public class FunctionsTest {

    ArrayList<Tariff> tariffs = new ArrayList<Tariff>();
    UnlimitedTariff unlimitedTariff = new UnlimitedTariff(503, 20);
    SuperMaxTariff superMaxTariff = new SuperMaxTariff(400, 15);
    UltraTariff ultraTariff = new UltraTariff(100, 30);
    @Test
    public void getTotalNumberOfCustomers() {

        tariffs.add(unlimitedTariff);
        tariffs.add(superMaxTariff);
        tariffs.add(ultraTariff);

        int totalNumberOfCustomers = Functions.getTotalNumberOfCustomers(tariffs);
        Assert.assertEquals(100003, totalNumberOfCustomers);

    }

    @Test
    public void sortBySubscriptionFee() {

        tariffs.add(ultraTariff);
        tariffs.add(unlimitedTariff);
        tariffs.add(superMaxTariff);


        List<Tariff> tariffs1 = Functions.sortBySubscriptionFee(tariffs);

        Assert.assertEquals(superMaxTariff, tariffs1.get(0));

    }

    @Test
    public void searchByParameters() {

        tariffs.add(unlimitedTariff);
        tariffs.add(superMaxTariff);
        tariffs.add(ultraTariff);
        Tariff tariff = Functions.searchByParameters(tariffs,  18, 25, 500, 1000);

        Assert.assertEquals(unlimitedTariff, tariff);
    }
}